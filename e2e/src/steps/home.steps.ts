import {Before, Given, Then, When,} from '@cucumber/cucumber';

import { expect } from 'chai';

import { HomePage } from '../pages/home.page';

let page: HomePage;

Before(() => {
  page = new HomePage();
});

Given(/^I am on the home page$/, async () => {
  await page.navigateTo();
});

When(/^I do nothing$/, () => {});

Then(/^I should see the title$/, async () => {
  expect(await page.getTitleText()).to.equal('Welcome to Angular rightstart');
});
