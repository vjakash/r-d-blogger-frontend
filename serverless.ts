import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';
import * as fs from 'fs';

import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
import { renderModule, AppServerModule } from './src/main.server';

// The Express app is exported so that it can be used by serverless Functions.
// export function app(): express.Express {
export const app = express();
const distFolder = join(process.cwd(), 'dist/rightstart-angular/browser');
const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModule,
}));

app.set('view engine', 'html');
app.set('views', distFolder);

// Example Express Rest API endpoints
// server.get('/api/**', (req, res) => { });
// Serve static files from /browser
app.get('*.*', express.static(distFolder, {
  maxAge: '1y',
}));

// All regular routes use the Universal engine
//   server.get('*', (req, res) => {
//     res.render(indexHtml, { req, providers:
// [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
//   });
app.get('*', (req, res) => {
  fs.readFile(join(__dirname, '../browser/index.html'), (err, html) => {
    if (err) {
      throw err;
    }

    renderModule(AppServerModule, {
      document: html.toString(),
      url: req.url,
    }).then((htmlRes) => {
      res.send(htmlRes);
    });
  });
});
//   return server;
// }

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
//   run();
}

export * from './src/main.server';
