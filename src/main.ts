import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import Auth from '@aws-amplify/auth';
import { Amplify } from 'aws-amplify';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import awsconfig from './aws-exports';

Auth.configure({ ...awsconfig, ssr: true });
Amplify.configure({ ...awsconfig, ssr: true });

if (environment.production) {
  enableProdMode();
}

document.addEventListener('DOMContentLoaded', () => {
  platformBrowserDynamic().bootstrapModule(AppModule).catch((err) => console.error(err));
});
