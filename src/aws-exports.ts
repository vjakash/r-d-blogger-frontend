import { environment } from './environments/environment';

const awsconfig = {
  aws_project_region: 'ap-south-1',
  aws_cognito_region: 'ap-south-1',
  // aws_user_pools_id: 'us-east-1_vcDd9klza',
  aws_user_pools_id: 'ap-south-1_wiaVl6QAm',
  aws_user_pools_web_client_id: '4mi8qbd6nhsvjtq72m3l15otbd',
  oauth: {
    domain: 'blogger-app.auth.ap-south-1.amazoncognito.com',
    scope: ['email', 'openid', 'aws.cognito.signin.user.admin', 'profile', 'phone'],
    redirectSignIn: environment.redirectSignIn,
    redirectSignOut: environment.redirectSignOut,
    responseType: 'token',
  },
};
export default awsconfig;
