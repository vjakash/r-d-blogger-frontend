export const environment = {
  production: true,
  apiURL: 'https://n78lrbfyz6.execute-api.ap-south-1.amazonaws.com/prod/api',
  redirectSignIn: 'https://d3an0gbhwh9rx5.cloudfront.net',
  // redirectSignIn: 'https://dev.d23ob33zidll8x.amplifyapp.com',
  redirectSignOut: 'https://d3an0gbhwh9rx5.cloudfront.net',
  // redirectSignOut: 'https://dev.d23ob33zidll8x.amplifyapp.com',
};
