import { NgModule } from '@angular/core';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzTabsModule } from 'ng-zorro-antd/tabs';

@NgModule({
  exports: [
    NzMessageModule,
    NzSkeletonModule,
    NzTabsModule,
  ],
})
export class ZorroAntModules {}
