import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http:HttpClient) { }

  getAllPosts():Observable<any> {
    return this.http.get(`${environment.apiURL}/posts/getAllPosts`);
  }

  getPostById(id:number):Observable<any> {
    return this.http.get(`${environment.apiURL}/posts/getPostById/${id}`);
  }

  postComment(data):Observable<any> {
    return this.http.post(`${environment.apiURL}/posts/postComment`, data);
  }

  createPost(data):Observable<any> {
    return this.http.post(`${environment.apiURL}/posts/createPost`, data);
  }

  likePost(data):Observable<any> {
    return this.http.post(`${environment.apiURL}/posts/likePost`, data);
  }

  followUser(data):Observable<any> {
    return this.http.post(`${environment.apiURL}/users/followUser`, data);
  }

  getUserById(id:number):Observable<any> {
    return this.http.get(`${environment.apiURL}/users/getUserById/${id}`);
  }
}
