/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable max-len */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Auth from '@aws-amplify/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router:Router) { }

  logout() {
    Auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  async canAccess() {
    try {
      const user = await Auth.currentAuthenticatedUser();
      return user.attributes.email;
    } catch (error) {
      return false;
    }
  }
}
