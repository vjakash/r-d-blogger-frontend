export enum AppRoutes {
  Login = 'login',
  Home = 'home',
  Profile = 'profile',
  Blog = 'blog',
  Create_Blog = 'createblog',
}
