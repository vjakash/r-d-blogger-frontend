/* eslint-disable no-param-reassign */
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import Auth from '@aws-amplify/auth';

@Injectable()
export class HttpInterceptorInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    return from(this.handle(request, next));
  }

  async handle(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Promise<HttpEvent<unknown>> {
    const data = await Auth.currentAuthenticatedUser().catch((err) => console.log(err));
    if (!request.headers.has('AWSAccessKeyId') && data) {
      request = request.clone({
        setHeaders: {
          Authorization: `${data.signInUserSession.idToken.jwtToken}`,
        },
      });
    }
    return next.handle(request).toPromise();
  }
}
