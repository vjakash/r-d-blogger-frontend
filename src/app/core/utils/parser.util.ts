export const parse = (data:string) => {
  // console.log('before', data);

  let processedData = data;
  data.replace(/\\n/g, '\\n')
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, '\\&')
    .replace(/\\r/g, '\\r')
    .replace(/\\t/g, '\\t')
    .replace(/\\b/g, '\\b')
    .replace(/\\f/g, '\\f');
  // eslint-disable-next-line no-control-regex
  processedData = processedData.replace(/[\u0000-\u0019]+/g, '');
  // processedData = data;
  // console.log('after', processedData);

  return JSON.parse(processedData);
};
