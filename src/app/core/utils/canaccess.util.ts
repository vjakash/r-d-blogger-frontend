/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-unused-vars */
import Auth from '@aws-amplify/auth';

export const canAccess = async () => {
  try {
    const user = await Auth.currentAuthenticatedUser();
    return user.attributes.email;
  } catch (error) {
    return false;
  }
};
// const isSignedIn = localStorage.getItem('isSignedIn');
// if (isSignedIn && JSON.parse(isSignedIn).isSingedIn) {
//   return JSON.parse(isSignedIn).email;
// }
// return false;
