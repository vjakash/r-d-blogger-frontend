export interface Blog{
  id:number;
  title:string;
  created_at:number;
  post:string;
  users:User;
  cover_image:string;
  likes_likesToposts:Like[];
  comments:Comment[];
}
export interface User{
  id:number;
  email:string;
  name:string;
  image:string;
  followers_followers_followingTousers:User[];
  followers_followers_followed_byTousers:User[];
  posts?:Blog[];
}
export interface Like{
  id:number;
  users:User
}
export interface Comment{
  id:number;
  comment:string,
  users:User;
  created_at:number;
}
