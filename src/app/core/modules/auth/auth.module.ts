import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZorroAntModules } from 'src/app/Ant/ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ZorroAntModules,
  ],
})
export class AuthModule { }
