/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-useless-constructor */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Auth, { CognitoHostedUIIdentityProvider } from '@aws-amplify/auth';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  signin=true;

  signInForm;

  signUpForm;

  // password='12345';

  // email='vjbakash@gmail.com';

  constructor(private fb:FormBuilder, private toastServ:NzMessageService, private router:Router) {
    this.signInForm = fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
    this.signUpForm = fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  /**
   * shows signup form
   */
  createOne() {
    this.signin = false;
    this.signInForm.reset();
  }

  /**
   * shows signin form
   */
  toggleSignIn() {
    this.signin = true;
    this.signUpForm.reset();
  }

  /**
   * Signs-in user when form is valid
   */
  signIn():void {
    if (this.signInForm.valid) {
      Auth.signIn({
        username: this.signInForm.value.email,
        password: this.signInForm.value.password,
      }).then((res) => {
        this.signInForm.reset();
        this.router.navigate(['/home']);
      }).catch((err) => {
        this.toastServ.error(err.message);
      });
    } else if (this.signInForm.controls.email.errors?.required) {
      this.toastServ.error('Enter Email');
    } else if (this.signInForm.controls.email.errors?.email) {
      this.toastServ.error('Enter valid email');
    } else if (this.signInForm.controls.password.errors?.required) {
      this.toastServ.error('Enter Password');
    }
  }

  /**
   * Signs-up user and send verification mail when form is valid
   */
  signUp() {
    if (this.signUpForm.valid) {
      Auth.signUp({
        username: this.signUpForm.value.email,
        password: this.signUpForm.value.password,
        attributes: {
          email: this.signUpForm.value.email,
          name: this.signUpForm.value.name,
        },
      }).then((res) => {
        this.toastServ.success(`Verification mail sent to ${this.signUpForm.value.email}`);
        this.signin = true;
        this.signUpForm.reset();
      }, (err) => {
        this.toastServ.error(err.message);
        console.log(err);
      });
    } else if (this.signUpForm.controls.name.errors?.required) {
      this.toastServ.error('Enter Name');
    } else if (this.signUpForm.controls.email.errors?.required) {
      this.toastServ.error('Enter Email');
    } else if (this.signUpForm.controls.email.errors?.email) {
      this.toastServ.error('Enter valid email');
    } else if (this.signUpForm.controls.password.errors?.required) {
      this.toastServ.error('Enter Password');
    }
  }

  /**
   * Sign in with facebook federated signin
   */
  signInFacebook() {
    Auth.federatedSignIn({
      provider: CognitoHostedUIIdentityProvider.Facebook,
    });
  }

  /**
   * Sign in with google federated signin
   */
  signInGoogle() {
    Auth.federatedSignIn({
      provider: CognitoHostedUIIdentityProvider.Google,
    }).then((user) => {
      console.log(user);
    }).catch((err) => {
      console.log('error from google signin', err);
    });
  }
}
