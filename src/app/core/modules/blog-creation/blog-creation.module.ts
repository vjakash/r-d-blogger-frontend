import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { QuillModule } from 'ngx-quill';
import { ZorroAntModules } from 'src/app/Ant/ng-zorro.module';
import { BlogCreationRoutingModule } from './blog-creation-routing.module';
import { BlogCreationPageComponent } from './blog-creation-page/blog-creation-page.component';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
    BlogCreationPageComponent,
  ],
  imports: [
    CommonModule,
    BlogCreationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    QuillModule.forRoot(),
    SharedModule,
    ZorroAntModules,
  ],
})
export class BlogCreationModule { }
