import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogCreationPageComponent } from './blog-creation-page/blog-creation-page.component';

const routes: Routes = [{
  path: '',
  component: BlogCreationPageComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogCreationRoutingModule { }
