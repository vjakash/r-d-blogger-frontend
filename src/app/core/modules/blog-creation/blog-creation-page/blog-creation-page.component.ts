/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-useless-constructor */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthService } from 'src/app/core/services/auth.service';
// import { canAccess } from 'src/app/core/utils/canAccess.util';

@Component({
  selector: 'app-blog-creation-page',
  templateUrl: './blog-creation-page.component.html',
  styleUrls: ['./blog-creation-page.component.scss'],
})
export class BlogCreationPageComponent implements OnInit {
  htmlContent;

  blurred = false;

  focused = false;

  newBlog;

  coverImagePreviewURL='';

  hover_effect=false;

  isCreatingPost=false;

  canAccess=false;

  constructor(private fb:FormBuilder,
    private toastServ:NzMessageService,
    private apiServ:ApiService,
    private authServ:AuthService) {
    this.newBlog = this.fb.group({
      title: new FormControl({ value: '', disabled: this.isCreatingPost }, [Validators.required]),
      cover_image: new FormControl({ value: '', disabled: this.isCreatingPost }, [Validators.required]),
      post: new FormControl({ value: '', disabled: this.isCreatingPost }, [Validators.required]),
    });
    authServ.canAccess().then((res) => {
      this.canAccess = res;
    });
  }

  ngOnInit(): void {
  }

  setInititalValue() {
    this.coverImagePreviewURL = '';
    this.newBlog.reset();
  }

  /**
   * publish a new blog if the form is valid
   */
  publish() {
    if (this.newBlog.valid) {
      this.isCreatingPost = true;
      this.toastServ.loading('Publishing', { nzDuration: 1000 });
      this.apiServ.createPost({
        ...this.newBlog.value,
        user_email: this.canAccess,
      }).subscribe((res) => {
        this.isCreatingPost = false;
        this.toastServ.success(res.body);
        this.setInititalValue();
      }, (err) => {
        this.isCreatingPost = false;
        console.log(err);
        this.toastServ.error('Error occured, try again');
      });
    } else {
      this.toastServ.warning('Please fill all the fields');
    }
  }

  /**
   *
   * select banner image
   * @param event file change event
   */
  selectFile(event) {
    this.newBlog.controls.cover_image.setValue(event.target.files[0]);
    this.previewImage();
  }

  /**
   * unselect selected banner image
   */
  unselectFile() {
    if (!this.isCreatingPost) {
      this.newBlog.controls.cover_image.setValue('');
      this.coverImagePreviewURL = '';
    }
  }

  /**
   * activate drag hover effect
   * @param ev drag over event
   */
  allowDrop(ev: Event): void {
    this.hover_effect = true;
    ev.preventDefault();
  }

  /**
   * remove drag hover effect
   * @param ev drag exit event
   */
  dragExitHandler(event: Event): void {
    event.preventDefault();
    this.hover_effect = false;
  }

  /**
   * select banner image from dropped file if the file is an image
   * @param event drop event
   */
  selectDroppedFile(event: any): void {
    event.preventDefault();
    let file = null;
    this.dragExitHandler(event);
    if (event.dataTransfer.items) {
      if (event.dataTransfer.items[0].kind === 'file') {
        file = event.dataTransfer.items[0].getAsFile();
      }
    } else {
      file = event.dataTransfer.files[0].getAsFile();
    }
    if (file.type.indexOf('image/') > -1) {
      this.newBlog.controls.cover_image.setValue(file);
      this.previewImage();
    } else {
      this.toastServ.warning('Please select an image');
    }
  }

  /**
   * convert image to base64 for preview
   */
  previewImage() {
    const reader = new FileReader();
    reader.onload = () => {
      this.coverImagePreviewURL = reader.result as string;
      this.newBlog.controls.cover_image.setValue(this.coverImagePreviewURL);
    };
    reader.readAsDataURL(this.newBlog.controls.cover_image.value);
  }
}
