/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-useless-constructor */
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-nav-top',
  templateUrl: './nav-top.component.html',
  styleUrls: ['./nav-top.component.scss'],
})
export class NavTopComponent implements OnInit {
  canAccess:boolean|string=false;

  hamMenuOpen=false;

  constructor(private authServ:AuthService) {
    authServ.canAccess().then((res) => {
      this.canAccess = res;
    });
  }

  ngOnInit(): void {
  }

  logout() {
    this.authServ.logout();
  }

  toggleHam() {
    this.hamMenuOpen = !this.hamMenuOpen;
  }
}
