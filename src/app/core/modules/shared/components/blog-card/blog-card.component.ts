/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-useless-constructor */
import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'src/app/core/models/blog.model';
import { parse } from '../../../../utils/parser.util';
@Component({
  selector: 'app-blog-card',
  templateUrl: './blog-card.component.html',
  styleUrls: ['./blog-card.component.scss'],
})
export class BlogCardComponent implements OnInit {
  @Input() blog!:Blog;

  parse=parse;

  constructor() { }

  ngOnInit(): void {
  }
}
