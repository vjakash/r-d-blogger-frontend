import { NgModule } from '@angular/core';

import { QuillModule } from 'ngx-quill';
import { CommonModule } from '@angular/common';
import { NavTopComponent } from './components/nav-top/nav-top.component';
import { SharedRoutingModule } from './shared-routing.module';
import { BlogCardComponent } from './components/blog-card/blog-card.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { CommentCardComponent } from './components/comment-card/comment-card.component';

@NgModule({
  declarations: [
    NavTopComponent,
    BlogCardComponent,
    UserCardComponent,
    CommentCardComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    QuillModule.forRoot(),
  ],
  exports: [
    NavTopComponent,
    BlogCardComponent,
    UserCardComponent,
    CommentCardComponent,
  ],
})
export class SharedModule { }
