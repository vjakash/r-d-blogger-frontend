import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ZorroAntModules } from 'src/app/Ant/ng-zorro.module';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogPageComponent } from './blog-page/blog-page.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    BlogPageComponent,
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    QuillModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ZorroAntModules,
  ],
})
export class BlogModule { }
