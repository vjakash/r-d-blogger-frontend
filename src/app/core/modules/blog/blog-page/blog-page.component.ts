import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Blog } from 'src/app/core/models/blog.model';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { parse } from '../../../utils/parser.util';

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.scss'],
})
export class BlogPageComponent {
  blog:Blog;

  id;

  comment='';

  parse=parse;

  is_loading=false;

  isPostingComment=false;

  openComments=false;

  debouncing;

  isFollowing=false;

  hasLiked=false;

  canAccess=false;

  constructor(private apiServ:ApiService,
    private activeRoute:ActivatedRoute,
    private toastServ:NzMessageService,
    private authServ:AuthService,
    private title:Title,
    private meta:Meta) {
    activeRoute.params.subscribe((data) => {
      this.id = data.id;
      this.getBlog();
    }, (err) => {
      console.log(err);
    });
    authServ.canAccess().then((res) => {
      this.canAccess = res;
    });
  }

  /**
   * get blog with id
   */
  getBlog():void {
    this.is_loading = true;
    this.apiServ.getPostById(this.id).subscribe((res) => {
      this.blog = res.body;
      this.title.setTitle(`${this.blog.title}-Story Book`);
      this.meta.addTags([
        { name: 'description', content: this.blog.title },
        { name: 'og:title', content: this.blog.title, property: 'og:title' },
        { name: 'twitter:card', content: this.blog.title },
        { name: 'image', content: this.blog.cover_image },
        { name: 'posted', content: new Date(this.blog.created_at).toUTCString() },
      ]);
      this.is_loading = false;
      this.processFollowers();
    }, (err) => {
      console.log(err);
      this.is_loading = false;
    });
  }

  /**
   * open comment section
   */
  openCommentSlider():void {
    this.openComments = true;
  }

  /**
   * close comment section
   */
  closeCommentSlider():void {
    this.openComments = false;
    this.comment = '';
  }

  stopPropagation(event:Event):void {
    event.stopPropagation();
  }

  /**
   * copy blog link to clipboard
   */
  share():void {
    navigator.clipboard.writeText(window.location.href);
    this.toastServ.success('Copied to clipboard');
  }

  /**
   * flattens user data
   * check if user has liked the post
   */
  processFollowers() {
    this.blog.users.followers_followers_followed_byTousers = this.blog.users
      .followers_followers_followed_byTousers
      .map((item:any) => ({ ...item.users_followers_followingTousers }));
    this.blog.users.followers_followers_followingTousers = this.blog.users
      .followers_followers_followingTousers
      .map((item:any) => ({ ...item.users_followers_followed_byTousers }));
    this.hasLiked = this.findUser(this.blog.likes_likesToposts);
  }

  /**
   * Like post if the user is signed in
   * api called with debouncing of 1s
   */
  like():void {
    this.hasLiked = !this.hasLiked;
    if (this.hasLiked) {
      this.blog.likes_likesToposts.push({
        id: -1,
        users: {
          id: -1, email: '', followers_followers_followed_byTousers: [], followers_followers_followingTousers: [], image: '', name: '',
        },
      });
    } else {
      this.blog.likes_likesToposts.pop();
    }
    if (this.canAccess) {
      clearTimeout(this.debouncing);
      this.debouncing = setTimeout(() => {
        this.apiServ.likePost({
          post_id: this.blog.id,
          user_email: this.canAccess,
          status: this.hasLiked,
        }).subscribe((res) => {
          this.blog = res.body;
          this.processFollowers();
        }, (err) => {
          console.log(err);
          this.blog.likes_likesToposts.pop();
        });
      }, 1000);
    } else {
      this.toastServ.info('Please Sign in to like');
    }
  }

  /**
   * post comment for the blog if the user is signed in
   */
  postComment():void {
    if (this.canAccess) {
      if (this.comment !== '') {
        this.isPostingComment = true;
        this.apiServ.postComment({
          post_id: this.blog.id,
          user_email: this.canAccess,
          comment: this.comment,
        }).subscribe((res) => {
          this.blog = res.body;
          this.processFollowers();
          this.comment = '';
          this.isPostingComment = false;
        }, (err) => {
          console.log(err);
          this.isPostingComment = false;
        });
      } else {
        this.toastServ.warning('Please type a comment');
      }
    } else {
      this.toastServ.info('Please Sign in to comment');
    }
  }

  /**
   * follows the author if the user is signed in
   */
  follow() {
    if (this.canAccess) {
      this.isFollowing = true;
      this.apiServ.followUser({
        author_id: this.blog.users.email,
        user_email: this.canAccess,
      }).subscribe((res) => {
        this.blog.users = res.body;
        this.processFollowers();
        this.isFollowing = false;
      }, (err) => {
        console.log(err);
        this.isFollowing = false;
      });
    } else {
      this.toastServ.info('Please Sign in to follow');
    }
  }

  /**
   * check if the signed in user is present in a list of users
   * @param list list to check in
   * @returns boolean
   */
  findUser(list) {
    return list.some((item) => {
      if (item.users && item.users.email === this.canAccess) {
        return true;
      } if (item.email === this.canAccess) {
        return true;
      }
      return false;
    });
  }
}
