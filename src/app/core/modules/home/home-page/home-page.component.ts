import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Auth from '@aws-amplify/auth';
import { Blog } from 'src/app/core/models/blog.model';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  blogs:Blog[]=[];

  is_loading=false;

  constructor(private router:Router, private apiServ:ApiService) {
    this.getData();
    Auth.currentAuthenticatedUser().then((user) => {
      console.log('home-page', user);
    });
  }

  /**
   * get all blogs
   */
  getData():void {
    this.is_loading = true;
    this.apiServ.getAllPosts().subscribe((res) => {
      this.blogs = res.body;
      this.is_loading = false;
    }, (err) => {
      console.log(err);
      this.is_loading = false;
    });
  }
}
