import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZorroAntModules } from 'src/app/Ant/ng-zorro.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProfilePageComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    ZorroAntModules,
  ],
})
export class ProfileModule { }
