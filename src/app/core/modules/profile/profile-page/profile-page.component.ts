/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-useless-constructor */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { User } from 'src/app/core/models/blog.model';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  canAccess=false;

  user:User;

  id;

  is_loading = false;

  isFollowing=false;

  constructor(private activeRoute:ActivatedRoute, private apiServ:ApiService,
    private authServ:AuthService,
    private toastServ:NzMessageService) {
    activeRoute.params.subscribe((data) => {
      this.id = data.id;
      this.getUser();
    }, (err) => {
      console.log(err);
    });
    authServ.canAccess().then((res) => {
      this.canAccess = res;
    });
  }

  ngOnInit(): void {
  }

  /**
 * get profile info
 */
  getUser() {
    this.is_loading = true;
    this.apiServ.getUserById(this.id).subscribe((res) => {
      this.user = res.body;
      this.id = this.user.id;
      this.is_loading = false;
      this.processFollowers();
    }, (err) => {
      console.log(err);
      this.is_loading = false;
    });
  }

  /**
   * flattens user data
   */
  processFollowers() {
    this.user.followers_followers_followed_byTousers = this.user
      .followers_followers_followed_byTousers
      .map((item:any) => ({ ...item.users_followers_followingTousers }));
    this.user.followers_followers_followingTousers = this.user
      .followers_followers_followingTousers
      .map((item:any) => ({ ...item.users_followers_followed_byTousers }));
  }

  /**
   * follows the author if the user is signed in
   */
  follow() {
    if (this.canAccess) {
      this.isFollowing = true;
      this.apiServ.followUser({
        author_id: this.id,
        user_email: this.canAccess,
      }).subscribe((res) => {
        this.user = res.body;
        this.processFollowers();
        this.isFollowing = false;
      }, (err) => {
        console.log(err);
        this.isFollowing = false;
      });
    } else {
      this.toastServ.info('Please Sign in to follow');
    }
  }

  /**
   * check if the signed in user is present in a list of users
   * @param list list to check in
   * @returns boolean
   */
  findUser(list) {
    return list.some((item) => {
      if (item.users && item.users.email === this.canAccess) {
        return true;
      } if (item.email === this.canAccess) {
        return true;
      }
      return false;
    });
  }
}
