import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from './core/constants/routes.constants';
import { AuthGuard } from './core/guards/auth.guard';
import { LoginGuardGuard } from './core/guards/login-guard.guard';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: AppRoutes.Login,
}, {
  path: AppRoutes.Login,
  pathMatch: 'full',
  loadChildren: () => import('./core/modules/auth/auth.module').then((mod) => mod.AuthModule),
  canActivate: [LoginGuardGuard],
}, {
  path: AppRoutes.Home,
  loadChildren: () => import('./core/modules/home/home.module').then((mod) => mod.HomeModule),
}, {
  path: AppRoutes.Blog,
  loadChildren: () => import('./core/modules/blog/blog.module').then((mod) => mod.BlogModule),
}, {
  path: AppRoutes.Profile,
  loadChildren: () => import('./core/modules/profile/profile.module').then((mod) => mod.ProfileModule),
}, {
  path: AppRoutes.Create_Blog,
  loadChildren: () => import('./core/modules/blog-creation/blog-creation.module').then((mod) => mod.BlogCreationModule),
  canActivate: [AuthGuard],
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
  })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
